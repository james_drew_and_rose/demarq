<?php

//Allowing featured image in posts
//add_theme_support( 'post-thumbnails' );

//Bring back menus
add_theme_support( 'menus' );

//Remove commenting section
add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;
    
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});

// Remove comments links from admin bar
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});
//END remove comment section

/* Register widget areas. */
function widget_registration() {

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		/*'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
		'after_title'   => '</h2>',*/
		'before_widget' => '',
		'after_widget'  => '',
	);
	// Footer #1.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Footer Left' ),
				'id'          => 'sidebar-1',
				'description' => __( 'Widgets in this area will be displayed in the left column in the footer.' ),
			)
		)
	);
	// Footer #2.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Footer Right' ),
				'id'          => 'sidebar-2',
				'description' => __( 'Widgets in this area will be displayed in the second column in the footer.' ),
			)
		)
	);
}

add_action( 'widgets_init', 'widget_registration' );

/* Menu locations */
function lc_menus() {
	$locations = array(
		'primary'  => __( 'Main Menu' ),
		'footer'  => __( 'Footer Menu' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'lc_menus' );


//Page Slug Body Class
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// Enable SVG
// Make sure that each SVG file starts with: <?xml version="1.0"...... etc
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// Make SVG visible in media upload
function custom_admin_head() {
	$css = '';
	$css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';
	echo '<style type="text/css">'.$css.'</style>';
}
add_action('admin_head', 'custom_admin_head');


?>