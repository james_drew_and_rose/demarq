<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>
<main id="site-content" role="main">
	<?php
	if ( have_posts() ) {while ( have_posts() ) {the_post();

			/*the_title( '<h1 class="entry-title">', '</h1>' );*/
			the_content();
			the_post_thumbnail( 'full' );

	}}?>
</main>


<?php get_footer(); ?>