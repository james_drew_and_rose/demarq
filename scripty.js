(function($) {

/* Useful:
addClass() - Adds one or more classes to the selected elements
removeClass() - Removes one or more classes from the selected elements
toggleClass() - Toggles between adding/removing classes from the selected elements
css() - Sets or returns the style attribute
html() - Content of the div
*/

/* Burger Nav */
$(".burger").click(function() {
	$(".burger").toggleClass("active");
	$(".nav.menu").toggleClass("active");
});


/* Code for slick slider */

$('.slider').slick({
    infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	variableWidth: true,
	dots:true,
	arrows:false,
 });
 

 var mainVideo = jQuery('#introvideo');
if (jQuery(window).width() < 600) {
	mainVideo.append('<video autoplay="" controls="" loop="" muted="" poster="https://oscars-on-it.demarq.com/wp-content/uploads/2022/05/poster-frame.jpg" playsinline="" id="introvideo"><source type="video/mp4" src="https://oscars-on-it.demarq.com/wp-content/uploads/2022/05/Demarq-mob-landing.mp4" /></video>');
	} else {
	mainVideo.append('<video autoplay="" controls="" loop="" muted="" poster="https://oscars-on-it.demarq.com/wp-content/uploads/2022/05/poster-frame.jpg" playsinline="" id="introvideo"><source type="video/mp4" src="https://oscars-on-it.demarq.com/wp-content/uploads/2022/05/demarq-landing-video-edited-1.mp4" /></video>');
	}


console.log( 'Welcome to Oscar, powered by Demarq' );
})( jQuery );
